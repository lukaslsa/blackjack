#include "Card_draw.h"


CD::CD (long bet)
{
    playerMoney=500000;
    pbet=bet;
    phand=0;
    dhand=0;
    createDeck();
}

void CD::createDeck()
{
    for (byte s=0; s<52; s++)
    {
        deck[s]=4;
    }
}

void CD::needToGetBetterSeed(unsigned long NewSeed)
{
    randomSeed(A0);
}

void CD::deal()
{
    byte handno=0;
    for (byte i=0; i<4; i++)
    {
        Serial.println(i);
        byte a;
        if(i%2==0)
        {
            a=0;
        }
        else a=1;
        while (0!=1)
        {
            randomCard = random(0,51);
            if(deck[randomCard]>=1)
            {
                if(a==0)
                {
                    phand++;
                }
                else dhand++;
                deck[randomCard]-=1;
                hand[a][handno]=randomCard;
                break;
            }
            else;
        }
        if(i%2==0)
        {
        }
        else handno++;
    }
    cardSum();
}

void CD::cardSum()
{
    for (int i=0; i<2; i++)
    {
        for(byte j=0; j<6; j++)
        {
            byte wsuit=0;
            byte temp = hand[i][j];
            if(temp>=13)
            {
                while(temp>13)
                {
                    temp-=13;
                    wsuit++;
                }
            }
            if(temp==0)
            {
                playedCardSum[i][0]+=temp+1;
                playedCardSum[i][1]+=temp+11;
            }
            else if(temp>=10 && temp<=12)
            {
                playedCardSum[i][0]+=10;
                playedCardSum[i][1]+=10;
            }
            else
            {
                playedCardSum[i][0]+=temp+1;
                playedCardSum[i][1]+=temp+1;
            }
        }
    }
}

void CD::bet(unsigned long pbet)
{
    bool chekker;
    deal();
    blackjack(chekker);
    if (chekker==true)
    {
        winner(chekker, pbet);
    }
    else 
    {

    }
}

bool CD::blackjack(bool BONUS)
{
    bool aceDetcted=false;
    bool aboveTenDetected=false;
    for (byte i=0; i<2; i++)
    {
        byte temp= hand[0][i];
        
        while(temp>13)
        {
            temp-=13;
        }
        if(temp==0)
        {
            aceDetcted=true;
        }
        else if(temp>=10 && temp<=12)
        {
            aboveTenDetected=true;
        }
        else;
    }
    if(aceDetcted==true && aboveTenDetected==true)
    {
        return true;
    }
    else return false;
}

void CD::phit()
{
    randomCard = random(0,51);
    if(deck[randomCard]>=1)
    {
        deck[randomCard]-=1;
        hand[0][phand]=randomCard;
        phand++;
    }
    else;
}

void CD::stand()
{
    cardSum();
    dhit();
    cardSum();
    witchCardSumIsBetterforPlayer();
    witchCardSumIsBetterforDealer();
    winner(false, pbet);
}

void CD::dhit()
{
    while(playedCardSum[1][0]<17 || playedCardSum[1][1]<17)
    {
        while (0!=1)
        {
            randomCard = random(0,51);
            if(deck[randomCard]==1)
            {
                deck[randomCard]=0;
                hand[1][dhand]=randomCard;
                dhand++;
                break;
            }
            else;
        }
    }
}
void CD::witchCardSumIsBetterforPlayer()
{
    if(playedCardSum[0][0]<=21)
    {
        if(playedCardSum[0][1]<=21)
        {
            if(playedCardSum[0][0]>playedCardSum[0][1])
            {
                cardSums[0]=playedCardSum[0][0];
            }
            else cardSums[0]=playedCardSum[0][1];
        }
        else cardSums[0]=playedCardSum[0][0];
    }
    else cardSums[0]=playedCardSum[0][1];
}

void CD::witchCardSumIsBetterforDealer()
{
    if(playedCardSum[1][0]<=21)
    {
        if(playedCardSum[1][1]<=21)
        {
            if(playedCardSum[1][0]>playedCardSum[1][1])
            {
                cardSums[1]=playedCardSum[1][0];
            }
            else cardSums[1]=playedCardSum[1][1];
        }
        else cardSums[1]=playedCardSum[1][0];
    }
    else cardSums[1]=playedCardSum[1][1];
}
void CD::winner(bool withBonus,unsigned long pbet)
{
    byte dcard;
    byte pcard;
    pcard=cardSums[0];
    dcard=cardSums[1];
    if(withBonus==1)
    {
        playerMoney+=pbet*1.5;
        state=4;
    }
    else
    {
        if(pcard>dcard)
        {
            playerMoney+=pbet;
            state=1;
        }
        if(cardSums[0]<cardSums[1])
        {
            playerMoney-=pbet;
            state=2;
        }
        if(cardSums[0]==cardSums[1])
        {
            playerMoney=playerMoney;
            state=3;
        }
    }
}