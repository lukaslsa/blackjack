#ifndef RNG_H
#define RNG_H

#include <Arduino.h>

class RNG
{
    public:
    RNG(int pinNumber);
    void createNewSeed();
    void createNewNumber();
    uint32_t showSeed(unsigned long showS);
    private:
    unsigned long rngseed;

};
#endif